<?php

namespace Acme\DemoBundle\Controller;

use Acme\DemoBundle\Entity\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Acme\DemoBundle\Form\ContactType;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DemoController extends Controller
{
    /**
     * @Route("/", name="_demo")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $is_send = $request->get('contact', null) ? 1 : 0;

        if ($is_send){
            /** @var Mailer $mailer */
            $mailer = new Mailer();
            $form = $this->createForm(new ContactType(), $mailer);
            $form->handleRequest($request);
            if ($form->isValid()) {

                $em = $this->getDoctrine()->getManager();

                $subject = 'Dear '.$mailer->getName().', here is the email body for you:';
                $message = \Swift_Message::newInstance($subject,$mailer->getText(),'text/html');
                $message
                    ->setTo($mailer->getMail(), $mailer->getName())
                    ->setFrom('admin@needja.net', 'Mailer test app');

                if(!$this->get('mailer')->send($message)){
                    return array('form'=>$form->createView());
                }
                $em->persist($mailer);
                $em->flush();

                $request->getSession()->getFlashBag()->set('notice', 'Message sent!');

                return new RedirectResponse($this->generateUrl('_thank'));
            }

            return array('form'=>$form->createView());
        }
        $form = $this->createForm(new ContactType());
        return array('form'=>$form->createView());

    }

    /**
     * @Route("/thank",name="_thank")
     * @Template()
     */
    public function thankAction(){

        return $this->render('AcmeDemoBundle:Demo:thank.html.twig');
    }
}
