<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 16.09.14
 * Time: 19:01
 */

namespace Acme\DemoBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Mailer
 * @package Acme\DemoBundle
 *
 * @ORM\Entity
 * @ORM\Table()
 */
class Mailer {

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="This field is required")
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank(message="This field is required")
     * @Assert\Email(
     *     message = "This value '{{ value }}' is not a valid email.",
     *     checkMX = true,
     *     checkHost = true
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $mail;

    /**
     * @var string
     * @Assert\NotBlank(message="This field is required")
     * @ORM\Column(type="text")
     */
    private $text;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Mailer
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Mailer
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Mailer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
